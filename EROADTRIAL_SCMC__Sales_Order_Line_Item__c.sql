SELECT DISTINCT
-- ISNULL(SalesOrderLine.CREATEDBYID,'') AS CREATEDBYID, -- uncomment this line if Users have been loaded
ISNULL(SalesOrderLine.CREATEDDATE,'') AS CREATEDDATE,
ISNULL(SalesOrderLine.CURRENCYISOCODE,'') AS CURRENCYISOCODE,
ISNULL(SalesOrderLine.ID,'') AS MIGRATIONID__C,
ISNULL(SalesOrderLine.ISDELETED,'') AS ISDELETED,
-- ISNULL(SalesOrderLine.LASTMODIFIEDBYID,'') AS LASTMODIFIEDBYID, -- uncomment this line if Users have been loaded
ISNULL(SalesOrderLine.LASTMODIFIEDDATE,'') AS LASTMODIFIEDDATE,
ISNULL(SCMC__ITEM_DESCRIPTION__C,'') AS DESCRIPTION,
ISNULL(SCMC__ORIGINAL_SALES_ORDER_LINE_ITEM__C,'') AS ORIGINALORDERITEMID,
ISNULL(SCMC__PRICE__C,'') AS LISTPRICE,
ISNULL(SCMC__PRICE__C,'') AS SBQQ__QUOTEDLISTPRICE__C,
ISNULL(SCMC__PRICE__C,'') AS TOTALPRICE,
ISNULL(SCMC__PRICE__C,'') AS UNITPRICE,
ISNULL(SCMC__QUANTITY_ALLOCATED__C,'') AS AVAILABLEQUANTITY,
ISNULL(SalesOrderLine.SCMC__QUANTITY__C,'') AS QUANTITY,
ISNULL(SalesOrderLine.SCMC__QUANTITY__C,'') AS SBQQ__ORDEREDQUANTITY__C,
ISNULL(SalesOrderLine.SCMC__QUANTITY__C,'') AS SBQQ__QUOTEDQUANTITY__C,
ISNULL(SalesOrderLine.SCMC__SALES_ORDER__C,'') AS ORDERID,
ISNULL(SalesOrderLine.SYSTEMMODSTAMP,'') AS SYSTEMMODSTAMP,

-- Below assigned values are mostly assumed to enable a successful load. Mapping from current Sales Order process to CPQ process needs to be done to derive proper values.
ISNULL(ServiceContractLine.SCMC__SERVICE_CONTRACT_LINE_START_DATE__C, '') AS ENDDATE,
'Monthly' AS SBQQ__BILLINGFREQUENCY__C,
ISNULL('Advance', '') AS SBQQ__BILLINGTYPE__C, 
ISNULL('Recurring','') AS SBQQ__CHARGETYPE__C,
ISNULL('New','') AS SBQQ__CONTRACTACTION__C,
-- ISNULL(ServiceContractLine.SCMC__SERVICE_ORDER__C,'') AS SBQQ__CONTRACT__C, -- this line needs the Contract records loaded

ISNULL('True','') AS SBQQ__CONTRACTED__C,
ISNULL('Inherit','') AS SBQQ__CONTRACTINGMETHOD__C,
ISNULL('1','') AS SBQQ__DEFAULTSUBSCRIPTIONTERM__C,
ISNULL('','') AS SBQQ__DIMENSIONTYPE__C,
-- ISNULL('','') AS SBQQ__PRICEDIMENSION__C, -- need to ensure Price Dimension has been loaded first
ISNULL('List','') AS SBQQ__PRICINGMETHOD__C,
ISNULL('Renewable','') AS SBQQ__PRODUCTSUBSCRIPTIONTYPE__C,
ISNULL('36','') AS SBQQ__PRORATEMULTIPLIER__C,
ISNULL('Activated','') AS SBQQ__STATUS__C,
ISNULL('Fixed Price','') AS SBQQ__SUBSCRIPTIONPRICING__C,
ISNULL('Renewable','') AS SBQQ__SUBSCRIPTIONTYPE__C,
ISNULL(ServiceContractLine.SCMC__Service_Order_Line_Cancel_Date__c,'') AS SBQQ__TERMINATEDDATE__C,
ISNULL('Rental','') AS SALES_METHOD__C, -- need transformation of Sales Order Type, default to Rental for now
ISNULL(ServiceContractLine.SCMC__Service_Contract_Line_Start_Date__c,'') AS SERVICEDATE,
'a1Z0l000000WcNREA0' AS BLNG__BILLINGRULE__C, -- set to equivalent environment ID ('a1Z0p0000004NnEEAU' in Dataload)
ISNULL('No','') AS BLNG__HOLDBILLING__C,
ISNULL('Pending Billing','') AS BLNG__INVOICERUNPROCESSINGSTATUS__C, -- default to Pending Billing for now, need to understand equivalent in SCMC
'a2I0l000000LizDEAS' AS BLNG__REVENUERECOGNITIONRULE__C, -- set to equivalent environment ID ('a2I0p0000004Qz3EAE' in Dataload)
'a2Q0l000000OIU7EAO' AS BLNG__TAXRULE__C, -- set to equivalent environment ID ('a2Q0p0000004FDoEAM' in Dataload)

-- Mapping from old PriceBookEntryId to new
CASE 
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000kJjqEAE' THEN '01u0l000002yCsyAAE'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000kJk5EAE' THEN '01u0l000002yDTfAAM'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000GoNwEAK' THEN '01u0l000003JO6sAAG'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000GoOiEAK' THEN '01u0l000003JMFxAAO'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000GoNyEAK' THEN '01u0l000003JO6tAAG'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000kJjvEAE' THEN '01u0l000002yCstAAE'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000H3T4EAK' THEN '01u0l000003JO7GAAW'
    ELSE ''
END AS PRICEBOOKENTRYID,

-- Mapping from old product to new
CASE 
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000kJjqEAE' THEN '01t0l0000030GJeAAM'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000kJk5EAE' THEN '01t0l000002wx6pAAA'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000GoNwEAK' THEN '01t0l00000332iPAAQ'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000GoOiEAK' THEN '01t0l00000332hsAAA'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000GoNyEAK' THEN '01t0l00000332iQAAQ'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000kJjvEAE' THEN '01t0l0000030GJZAA2'
    WHEN SalesOrderLine.SCMC__ITEM_MASTER__C = 'a4G90000000H3T4EAK' THEN '01t0l00000332j0AAA'
    ELSE ''
END AS PRODUCT2ID


FROM EROADTRIAL.dbo.SCMC__Sales_Order_Line_Item__c as SalesOrderLine
INNER JOIN EROADTRIAL.dbo.SCMC__Sales_Order__c as SalesOrder
ON SalesOrder.ID = SalesOrderLine.SCMC__SALES_ORDER__C
LEFT JOIN EROADTRIAL.dbo.SCMC__Service_Order__c as ServiceContract
ON ServiceContract.SCMC__SALES_ORDER__C = SalesOrder.ID
LEFT JOIN EROADTRIAL.dbo.SCMC__Service_Order_Line__c as ServiceContractLine
ON ServiceContractLine.SCMC__SERVICE_ORDER__C = ServiceContract.ID
WHERE SalesOrder.ID in ('a520o000000lI1PAAU', 
'a520o000000lI1AAAU', 
'a520o000000lI10AAE', 
'a520o000000lI0qAAE', 
'a520o000000lI0lAAE', 
'a520o000000lI0bAAE', 
'a520o000000lI0WAAU', 
'a520o000000lI0RAAU', 
'a520o000000lI0MAAU', 
'a520o000000lI0HAAU')
GO

-- Next Step: SBQQ__Contract__c field to be updated once Contracts have been loaded


-- SELECT DISTINCT
-- -- SalesOrderLine.ID,
-- SalesOrderLine.SCMC__ITEM_DESCRIPTION__C,
-- SalesOrderLine.SCMC__ITEM_MASTER__C,
-- SalesOrderLine.ITEM_NAME__C
-- FROM EROADTRIAL.dbo.SCMC__Sales_Order_Line_Item__c as SalesOrderLine
-- INNER JOIN EROADTRIAL.dbo.SCMC__Sales_Order__c as SalesOrder
-- ON SalesOrderLine.SCMC__SALES_ORDER__C = SalesOrder.ID
-- LEFT JOIN EROADTRIAL.dbo.SCMC__Service_Order__c as ServiceContract
-- ON SalesOrderLine.SCMC__SALES_ORDER__C = ServiceContract.SCMC__SALES_ORDER__C
-- LEFT JOIN EROADTRIAL.dbo.SCMC__Service_Order_Line__c as ServiceContractLine
-- ON ServiceContractLine.SCMC__SERVICE_ORDER__C = ServiceContract.ID
-- WHERE SalesOrder.ID in ('a520o000000lI1PAAU', 
-- 'a520o000000lI1AAAU', 
-- 'a520o000000lI10AAE', 
-- 'a520o000000lI0qAAE', 
-- 'a520o000000lI0lAAE', 
-- 'a520o000000lI0bAAE', 
-- 'a520o000000lI0WAAU', 
-- 'a520o000000lI0RAAU', 
-- 'a520o000000lI0MAAU', 
-- 'a520o000000lI0HAAU')
-- -- GROUP BY SalesOrderLine.ID
-- -- ORDER BY count(*) desc
-- GO