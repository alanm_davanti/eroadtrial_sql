
-- simple address cleansing to match Account Address
UPDATE t1
SET t1.MAILINGCOUNTRY = t2.BILLINGCOUNTRY,
t1.MAILINGSTATE = t2.BILLINGSTATE
FROM EROADTRIAL.dbo.Contact as t1
INNER JOIN EROADTRIAL.dbo.Account as t2
ON t1.ACCOUNTID = t2.ID
WHERE t1.MAILINGADDRESS = ''
AND t1.MAILINGCITY = ''
AND t1.MAILINGCOUNTRY = ''
AND t1.MAILINGGEOCODEACCURACY = ''
AND t1.MAILINGLATITUDE = ''
AND t1.MAILINGLONGITUDE = ''
AND t1.MAILINGPOSTALCODE = ''
AND t1.MAILINGSTATE = ''
AND t1.MAILINGSTREET = ''
GO

-- select records for loading to Contact
SELECT 
ACCOUNTID as ACCOUNTID,
ASSISTANTNAME as ASSISTANTNAME,
ASSISTANTPHONE as ASSISTANTPHONE,


BIRTHDATE as BIRTHDATE,


ISNULL(CONTACT_ID__C,'') as DEPOT_ID__C,


CREATEDBYID as CREATEDBYID,
CREATEDDATE as CREATEDDATE,
CURRENCYISOCODE as CURRENCYISOCODE,

DEPARTMENT as DEPARTMENT,
DEPOT_USER_ACTIVE__C as DEPOT_USER_ACTIVE__C,
DEPOT_USERNAME__C as DEPOT_USERNAME__C,
DESCRIPTION as DESCRIPTION,
DONOTCALL as DONOTCALL,
EMAIL as EMAIL,
EMAILBOUNCEDDATE as EMAILBOUNCEDDATE,
EMAILBOUNCEDREASON as EMAILBOUNCEDREASON,
FAX as FAX,
FIRSTNAME as FIRSTNAME,

HASOPTEDOUTOFEMAIL as HASOPTEDOUTOFEMAIL,
HASOPTEDOUTOFFAX as HASOPTEDOUTOFFAX,

HOMEPHONE as HOMEPHONE,
ID as MIGRATIONID__C,
INDIVIDUALID as INDIVIDUALID,






ISDELETED as ISDELETED,
ISEMAILBOUNCED as ISEMAILBOUNCED,
JIGSAW as JIGSAW,
JIGSAWCONTACTID as JIGSAWCONTACTID,




LASTACTIVITYDATE as LASTACTIVITYDATE,
LASTCUREQUESTDATE as LASTCUREQUESTDATE,
LASTCUUPDATEDATE as LASTCUUPDATEDATE,
LASTMODIFIEDBYID as LASTMODIFIEDBYID,
LASTMODIFIEDDATE as LASTMODIFIEDDATE,
LASTNAME as LASTNAME,
LASTREFERENCEDDATE as LASTREFERENCEDDATE,
LASTVIEWEDDATE as LASTVIEWEDDATE,
LEADSOURCE as LEADSOURCE,
MAILINGADDRESS as MAILINGADDRESS,
MAILINGCITY as MAILINGCITY,
MAILINGCOUNTRY as MAILINGCOUNTRY,
MAILINGGEOCODEACCURACY as MAILINGGEOCODEACCURACY,
MAILINGLATITUDE as MAILINGLATITUDE,
MAILINGLONGITUDE as MAILINGLONGITUDE,
MAILINGPOSTALCODE as MAILINGPOSTALCODE,
MAILINGSTATE as MAILINGSTATE,
MAILINGSTREET as MAILINGSTREET,
MASTERRECORDID as MASTERRECORDID,
ISNULL(MIDDLE_NAME__C,'') as MIDDLENAME,

MOBILEPHONE as MOBILEPHONE,
NAME as NAME,
OPEN_CASES__C as OPEN_CASES__C,


OTHERADDRESS as OTHERADDRESS,
OTHERCITY as OTHERCITY,
OTHERCOUNTRY as OTHERCOUNTRY,
OTHERGEOCODEACCURACY as OTHERGEOCODEACCURACY,
OTHERLATITUDE as OTHERLATITUDE,
OTHERLONGITUDE as OTHERLONGITUDE,
OTHERPHONE as OTHERPHONE,
OTHERPOSTALCODE as OTHERPOSTALCODE,
OTHERSTATE as OTHERSTATE,
OTHERSTREET as OTHERSTREET,
OWNERID as OWNERID,
PHONE as PHONE,
PHOTOURL as PHOTOURL,























-- REPORTSTOID as REPORTSTOID, -- Lookup(Contact)

SALUTATION as SALUTATION,








SYSTEMMODSTAMP as SYSTEMMODSTAMP,

TITLE as TITLE

FROM EROADTRIAL.dbo.Contact
GO