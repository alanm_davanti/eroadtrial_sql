List<Asset> records = [select id, name, MigrationID__c, CreatedByID
                       from Asset
                             where MigrationID__c != null
                             AND CreatedById = '0050p000000roMaAAI'];
System.debug('records.size(): ' + records.size());
delete records;

List<PriceBook2> PriceBooks = [SELECT Id, Name, MigrationID__c
                              FROM PriceBook2];
for (PriceBook2 pb: PriceBooks) {
    if (pb.Name == 'Standard Price Book')
        pb.MigrationID__c = '01s0o00000CAUmhAAH';
    if (pb.Name == 'EROAD (Service)')
        pb.MigrationID__c = '01s0l000000kvALAAY';
    if (pb.Name == 'Future Product')
        pb.MigrationID__c = '01s0l000000kroyAAA';
    if (pb.Name == 'EROAD')
        pb.MigrationID__c = '01s0l000000gb3GAAQ';
}
update PriceBooks;


List<PriceBookEntry> records = [select id, name, MigrationID__c, CreatedByID
                       from PriceBookEntry
                             where MigrationID__c != null
                             and pricebook2id = '01s0p000000A8cIAAS'
                             AND CreatedById = '0050p000000roMaAAI'];
System.debug('records.size(): ' + records.size());
delete records;




// MigrationID copier: to be executed in Execute Anonymous


		// <members>Product2.MigrationID__c</members>
		// <members>SBQQ__ProductOption__c.MigrationID__c</members>
		// <members>PricebookEntry.MigrationID__c</members>
		// <members>blng__BillingRule__c.MigrationID__c</members>
		// <members>blng__RevenueRecognitionRule__c.MigrationID__c</members>
		// <members>blng__TaxRule__c.MigrationID__c</members>
		// <members>SBQQ__ProductFeature__c.MigrationID__c</members>


List<SBQQ__ProductOption__c> records = [SELECT Id, Name, MigrationID__c
                                FROM SBQQ__ProductOption__c
                                WHERE MigrationID__c = null];

for (SBQQ__ProductOption__c record: records) {
    record.MigrationID__c = record.Id;
}
System.debug('size: ' + records.size());
update records;

// dynamic query -- version 1
Set<String> objects = new Set<String>();
objects.add('Product2');
objects.add('SBQQ__ProductOption__c');
objects.add('PricebookEntry');
objects.add('blng__BillingRule__c');
objects.add('blng__RevenueRecognitionRule__c');
objects.add('blng__TaxRule__c');
objects.add('SBQQ__ProductFeature__c');
objects.add('SBQQ__Dimension__c');
System.debug(objects);

Map<String, List<sObject>> objToUpdate = new Map<String, List<sObject>>();
Integer updateCount = 0;

for (String obj: objects) {
    String myQuery = 'SELECT Id, Name, MigrationID__c FROM ' 
        + obj + ' WHERE MigrationID__c = null';
    List<sObject> queryResult = Database.query(myQuery);
    Integer numRecords = queryResult.size();
    System.debug(obj + ' : ' + numRecords);
    objToUpdate.put(obj, queryResult);
    updateCount = updateCount + numRecords;
}

if (updateCount > 0) {
List<sObject> objsMigrationIDUpdate = new List<sObject>();

for (String obj: objToUpdate.keySet()) {
    if (obj == 'Product2') {
        List<Product2> records = objToUpdate.get('Product2');
        for (Product2 record: records) {
            record.MigrationID__c = record.Id;
            objsMigrationIDUpdate.add(record);
        }
        System.debug(obj + ' : ' + records.size() + ' records updated.');
    }
    else if (obj == 'SBQQ__ProductOption__c') {
        List<SBQQ__ProductOption__c> records = objToUpdate.get('SBQQ__ProductOption__c');
        for (SBQQ__ProductOption__c record: records) {
            record.MigrationID__c = record.Id;
            objsMigrationIDUpdate.add(record);
        }
        System.debug(obj + ' : ' + records.size() + ' records updated.');
    }
    else if (obj == 'PricebookEntry') {
        List<PricebookEntry> records = objToUpdate.get('PricebookEntry');
        for (PricebookEntry record: records) {
            record.MigrationID__c = record.Id;
            objsMigrationIDUpdate.add(record);
        }
        System.debug(obj + ' : ' + records.size() + ' records updated.');
    }
    else if (obj == 'blng__BillingRule__c') {
        List<blng__BillingRule__c> records = objToUpdate.get('blng__BillingRule__c');
        for (blng__BillingRule__c record: records) {
            record.MigrationID__c = record.Id;
            objsMigrationIDUpdate.add(record);
        }
        System.debug(obj + ' : ' + records.size() + ' records updated.');
    }
    else if (obj == 'blng__RevenueRecognitionRule__c') {
        List<blng__RevenueRecognitionRule__c> records = objToUpdate.get('blng__RevenueRecognitionRule__c');
        for (blng__RevenueRecognitionRule__c record: records) {
            record.MigrationID__c = record.Id;
            objsMigrationIDUpdate.add(record);
        }
        System.debug(obj + ' : ' + records.size() + ' records updated.');
    }
    else if (obj == 'blng__TaxRule__c') {
        List<blng__TaxRule__c> records = objToUpdate.get('blng__TaxRule__c');
        for (blng__TaxRule__c record: records) {
            record.MigrationID__c = record.Id;
            objsMigrationIDUpdate.add(record);
        }
        System.debug(obj + ' : ' + records.size() + ' records updated.');
    }
    else if (obj == 'SBQQ__ProductFeature__c') {
        List<SBQQ__ProductFeature__c> records = objToUpdate.get('SBQQ__ProductFeature__c');
        for (SBQQ__ProductFeature__c record: records) {
            record.MigrationID__c = record.Id;
            objsMigrationIDUpdate.add(record);
        }
        System.debug(obj + ' : ' + records.size() + ' records updated.');
    }
    else if (obj == 'SBQQ__Dimension__c') {
        List<SBQQ__Dimension__c> records = objToUpdate.get('SBQQ__Dimension__c');
        for (SBQQ__Dimension__c record: records) {
            record.MigrationID__c = record.Id;
            objsMigrationIDUpdate.add(record);
        }
        System.debug(obj + ' : ' + records.size() + ' records updated.');
    }
}

System.debug(objsMigrationIDUpdate.size() + ' records updated and committed to database.');
// update objsMigrationIDUpdate;

} else {
    System.debug('No record found. All MigrationID__c fields have been populated.');
}



/**********************dynamic query verion 2****************************/
Set<String> objects = new Set<String>();
objects.add('Product2');
objects.add('SBQQ__ProductOption__c');
objects.add('PricebookEntry');
objects.add('blng__BillingRule__c');
objects.add('blng__RevenueRecognitionRule__c');
objects.add('blng__TaxRule__c');
objects.add('SBQQ__ProductFeature__c');
objects.add('SBQQ__Dimension__c');
System.debug(objects);

List<sObject> recsToUpdate = new List<sObject>();
Integer updateCount = 0;

for (String obj: objects) {
    String myQuery = 'SELECT Id, Name, MigrationID__c FROM ' 
        + obj + ' WHERE MigrationID__c = null';
    List<sObject> queryResults = Database.query(myQuery);
    Integer numRecords = queryResults.size();
    System.debug(obj + ' : ' + numRecords);
    for (sObject oneRec: queryResults) {
    	recsToUpdate.add(oneRec);
        System.debug(obj + ' Id: ' + oneRec.Id + ';Name: ' + oneRec.get('name'));
    }
}

updateCount = recsToUpdate.size();
System.debug('recsToUpdate.size(): ' + updateCount);

try {
	if (updateCount > 0) {
    	for (sObject rec: recsToUpdate) {
            // System.debug('rec.get(MigrationId) before update: ' + rec.get('MigrationID__c'));
            // System.debug('rec.get(Id): ' + rec.get('Id'));
        	rec.put('MigrationID__c', rec.get('Id'));
            // System.debug('rec.get(MigrationId) after update: ' + rec.get('MigrationID__c'));
        }
        //update recsToUpdate;
    }else {
        System.debug('No record found. All MigrationID__c fields have been populated.');
    }
}catch (DMLException e) {
    System.debug('Error: ' + e.getMessage());
}
/**********************dynamic query verion 2****************************/







/*****************scratch *************************************/

Product2 prod2 = [SELECT Id, ProductCode
                 FROM Product2
                 WHERE Id = '01t0l0000032BCnAAM'];
String[] productCode = prod2.ProductCode.split('_');
System.debug('productCode:: ' + productCode);
System.debug('productCode.size():: ' + productCode.size());
String weightType = productCode[productCode.size() - 1];
System.debug('weightType:: ' + weightType);


System.debug(Asset.D365_Asset_Number__c);
Schema.DescribeFieldResult F = Asset.D365_Asset_Number__c.getDescribe();
System.debug('Schema.DescribeFieldResult:: ' + F);
Schema.sObjectField T = F.getSObjectField();
System.debug('Schema.sObjectField:: ' + T);

    
CaseEvent_SyncCase.CaseRecordWrapper caseEventWrap = new CaseEvent_SyncCase.CaseRecordWrapper();
caseEventWrap.isNew = false;
caseEventWrap.organisationNumber = 'O-0000055623';
Set<Id> caseIds = new Set<Id>();
caseIds.add('5000l000005pstVAAQ');
caseEventWrap.caseRecord = CaseService.getCasesByCaseIdSet(caseIds)[0];
caseEventWrap.eventType = 'UpdateContract';
List<CaseEvent_SyncCase.CaseRecordWrapper> caseEventWrapList = new List<CaseEvent_SyncCase.CaseRecordWrapper>();
caseEventWrapList.add(caseEventWrap);
IntegrationService.getInstance().synchronizeCase(caseEventWrapList);


Set<Id> caseIds = new Set<Id>();
caseIds.add('5000l000005nSOQAA2');
Case[] retrievedCaseList = CaseService.getCasesByCaseIdSet(caseIds);
system.debug(retrievedCaseList);
System.debug(retrievedCaseList[0]);
System.debug(retrievedCaseList[0].Account);
//System.debug(retrievedCaseList[0].Account.get('Name') + ' :: ' + retrievedCaseList[0].Account.get('BillingCountryCode'));

OrganisationWrapper orgWrapper = OrganisationWrapper.convertCaseToOrganisationWrapper(retrievedCaseList[0]);
System.debug(orgWrapper);
               
List<CRM_Integration__e> ieventList = new List<CRM_Integration__e>();
                ieventList.add(new CRM_Integration__e(
                    Message__c = JSON.serialize(orgWrapper), //Serialized Record
                    Object__c = 'RMACase',
                    Operation__c = 'U'
                ));

System.debug(ieventList);


Set<String> serialNumberSet = new Set<String>();
serialNumberSet.add('00001072_ybI_1');
serialNumberSet.add('00001072_WAw_1');
List<Asset> assets = new List<Asset>(AssetSelector.newInstance().getAssetBySerialNumberSet(serialNumberSet));
for (Asset ast: assets) {
    System.debug('ast.Current_Machine__c:: ' + ast.Current_Machine__c);
    System.debug(ast.Previous_Machine__c);
    System.debug(ast.AccountId);
    System.debug(ast.Account.Organisation_Number__c);
    System.debug(ast.SBQQ__OrderProduct__r.Pricebookentry.Product2Id);
}

QueryFactory AssetQueryFactory = newQueryFactory();
            
            AssetQueryFactory.selectField('Current_Machine__c');
            AssetQueryFactory.selectField('Previous_Machine__c');
            AssetQueryFactory.selectField('AccountId');
            AssetQueryFactory.selectField('Account.Organisation_Number__c');
            AssetQueryFactory.selectField('D365_Asset_Number__c');
            AssetQueryFactory.selectField('SBQQ__OrderProduct__r.Product2Id');

            AssetQueryFactory.setCondition('SerialNumber IN :serialNumberSet ' 
                                           + 'AND AccountID != null '
                                           + 'AND Account.Organisation_Number__c != null '
                                           + 'AND D365_Asset_Number__c != null ');

            return Database.query(AssetQueryFactory.toSOQL());



String toOrganizationId = '';
List<Order> lstOrder = [Select Id, AccountId, Account.Organisation_Number__c, SBQQ__Quote__c, 
                                        (Select Id, SBQQ__Subscription__c from OrderItems where Product_Family_Text__c = 'Plan' and Quantity > 0 limit 1) 
                                        from Order where id='8010l000000PnoTAAS' LIMIT 1];
                if(lstOrder.size() > 0){
                    toOrganizationId = lstOrder[0].AccountId;
                    System.debug('lstOrder:: ' + lstOrder);
                    System.debug(lstOrder[0].OrderItems[0]);
                    System.debug(lstOrder[0].OrderItems[0].Id);
                    System.debug(lstOrder[0].OrderItems[0].SBQQ__Subscription__c);
                }


            List<Asset> lstAsset = [select Id, AccountId, Status, Order__c, Current_Machine__c, Quote__c, SBQQ__OrderProduct__c, 
                                    lifeCycleState__c, D365_Asset_Number__c, SerialNumber, Service_Contract__c, 
                                    SBQQ__OrderProduct__r.PricebookEntry.Product2Id, SBQQ__CurrentSubscription__c 
                                    from Asset where Id = '02i0l000001tbQ8AAI'];
String seletedType = 'Transfer Device';
for(Asset asset: lstAsset){
                /*if(!seletedType.equals('Device Upgrade')){ // for Plan Upgrade and Transfer Device
                    asset.Order__c = amendmentOrder.Id;
                    asset.Quote__c = amendmentOrder.SBQQ__Quote__c;
                    if(amendmentOrder.OrderItems != null && amendmentOrder.OrderItems.size() > 0){
                        asset.SBQQ__OrderProduct__c = amendmentOrder.OrderItems[0].Id;
                        asset.SBQQ__CurrentSubscription__c = amendmentOrder.OrderItems[0].SBQQ__Subscription__c;
                    }
                }else{ // specifically for Device Upgrade
                    asset.Current_Machine__c = null;
                    asset.Status = 'RMA to be Returned';
                }*/
                
                if(seletedType.equals('Transfer Device')){ // for Transfer device, align asset to correct references
                    if(String.IsNotBlank(toOrganizationId)){
                        asset.AccountId = toOrganizationId;
                        asset.Order__c = lstOrder[0].Id;
                        asset.Quote__c = lstOrder[0].SBQQ__Quote__c;
                        if(lstOrder[0].OrderItems != null && lstOrder[0].OrderItems.size() > 0){
                            asset.SBQQ__OrderProduct__c = lstOrder[0].OrderItems[0].Id;
                            asset.SBQQ__CurrentSubscription__c = lstOrder[0].OrderItems[0].SBQQ__Subscription__c;
                        }
                    }
                    asset.Current_Machine__c = null;
                    asset.Status = 'Pending Assignment';
                    asset.lifeCycleState__c = 'Pending Assignment';
                    asset.D365_Asset_Number__c = lstOrder[0].Account.Organisation_Number__c + '_' + asset.SerialNumber;
                                            asset.Service_Contract__c = null;
                }
            }
System.debug(lstAsset);





// Code in TEST1 Dev Console (20191126)
System.abortJob('08e0l00000NNN2LAAX');

ContractRenewalSchedule.startSchedule();
ContractRenewalOrderSchedule.startSchedule();

ContractRenewalSchedule.stopSchedule();
ContractRenewalOrderSchedule.stopSchedule();

/*List<Account> insertedAccounts = new List<Account>([SELECT Id, Name, MigrationID__c
                                                   FROM Account
                                                   where MigrationId__c != null
                                                   LIMIT 7000]);
System.debug('insertedAccounts.size(): ' + insertedAccounts.size());
delete insertedAccounts; */

List<Contact> contacts = [SELECT Id, Name, MigrationID__c
                         FROM Contact
                         WHERE MigrationID__c != null
                         LIMIT 5000];
System.debug('contacts.size(): ' + contacts.size());
delete contacts;

CaseEvent_SyncCase.CaseRecordWrapper caseEventWrap = new CaseEvent_SyncCase.CaseRecordWrapper();
caseEventWrap.isNew = false;
caseEventWrap.organisationNumber = 'O-0000065112';
Set<Id> caseIds = new Set<Id>();
caseIds.add('5000l000005puexAAA');
caseEventWrap.caseRecord = CaseService.getCasesByCaseIdSet(caseIds)[0];
caseEventWrap.eventType = 'UpdateContract';
List<CaseEvent_SyncCase.CaseRecordWrapper> caseEventWrapList = new List<CaseEvent_SyncCase.CaseRecordWrapper>();
caseEventWrapList.add(caseEventWrap);
IntegrationService.getInstance().synchronizeCase(caseEventWrapList);



private Map<String, List<Product2>> productCodetoProductIdMap = new Map<String, List<Product2>>();
Set<String> productCodeSet = new Set<String>();
productCodeSet.add('NZ_PLAN_EH2_Connected');
productCodeSet.add('NZ_PLAN_EH2_ConnectedPlus');
List<Product2> queryResult = ProductService.getProductsForAssignedDeviceByProductCode(productCodeSet);
System.debug(queryResult.size());

        if (productCodeSet != null && productCodeSet.size() > 0) {
            //for (Product2 prod2 : [Select Id, ProductCode From Product2 Where ProductCode IN :productCodeSet]) {
            for (String extDepotCode: productCodeSet) {
                productCodetoProductIdMap.put(extDepotCode, new List<Product2>());
                //System.debug('productCodetoProductIdMap.get(extDepotCode) :: '+ productCodetoProductIdMap.get(extDepotCode));
                //productCodetoProductIdMap.get(extDepotCode).add(new Product2(Name = 'Alan Sample Product 1'));
                //productCodetoProductIdMap.get(extDepotCode).add(new Product2(Name = 'Alan Sample Product 2'));
                //System.debug('productCodetoProductIdMap.get(extDepotCode) :: '+ productCodetoProductIdMap.get(extDepotCode));
                
                /*for (Product2 prod2 : queryResult) {
                //this.productCodetoProductIdMap.put(prod2.ProductCode, prod2.Id);
                
                //2-Nov: Using Depot Productcodeinstead
                if (prod2.External_Depot_Code__c == extDepotCode) {
                	productCodetoProductIdMap.put(extDepotCode, productCodetoProductIdMap.get(extDepotCode).add(prod2));    
                }*/
                
            }
                    
            System.debug(productCodetoProductIdMap);
            //System.debug(productCodetoProductIdMap.keyset().get(0));
            //System.debug(productCodetoProductIdMap.keyset().get(1));
            //productCodetoProductIdMap.put(productCodetoProductIdMap.keyset()[0], productCodetoProductIdMap.get(productCodetoProductIdMap.keyset()[0]).add(queryResult[0]));
            
            for (Product2 prod2 : queryResult) {
                //this.productCodetoProductIdMap.put(prod2.ProductCode, prod2.Id);
                
                //2-Nov: Using Depot Productcodeinstead
             
                	productCodetoProductIdMap.get(prod2.External_Depot_Code__c).add(prod2);    
                               
            }
        }

/*for (Product2 prod2 : queryResult) {
    System.debug('prod2::' + prod2);
    productCodetoProductIdMap.put(prod2.External_Depot_Code__c, prod2.Id);
}*/
System.debug(productCodetoProductIdMap);
for (String prodCode: productCodetoProductIdMap.keySet()) {
    System.debug(productCodeToProductIdMap.get(prodCode).size());
}


// Contract Renewal to Evergreen
ContractRenewalBatch conrenbatch = new ContractRenewalBatch();
Id batchId = Database.executeBatch(conrenbatch, 200);

// Contract Renewal Order creation:
ContractRenewalOrderBatch renewalorderbatch = new ContractRenewalOrderBatch();
Id batchId = Database.executeBatch(renewalorderbatch, 200);

//Triggering TransferDevice / RMACase payload:
CaseEvent_SyncCase.CaseRecordWrapper caseRecWrap = new CaseEvent_SyncCase.CaseRecordWrapper();
//caseRecWrap.eventType = 'TransferDevice';
//caseRecWrap.caseRecord = [SELECT Id from Case where Id = '5000l000005qLWTAA2'];
caseRecWrap.eventType = 'RMACase';
caseRecWrap.caseRecord = [SELECT Id from Case where Id = '5000l000005qRtmAAE'];
caseRecWrap.isNew = false;
List<CaseEvent_SyncCase.CaseRecordWrapper> caseList = new List<CaseEvent_SyncCase.CaseRecordWrapper>();
caseList.add(caseRecWrap);
System.debug('caseList:: ' + caseList);
IntegrationService.getInstance().synchronizeCase(caseList);