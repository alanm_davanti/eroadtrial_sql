/*******************************************DELETING the FIELD*******************************************************/
Set<String> objects = new Set<String>();
objects.add('Product2');
objects.add('SBQQ__ProductOption__c');
objects.add('PricebookEntry');
objects.add('blng__BillingRule__c');
objects.add('blng__RevenueRecognitionRule__c');
objects.add('blng__TaxRule__c');
objects.add('SBQQ__ProductFeature__c');
objects.add('SBQQ__Dimension__c');
System.debug(objects);

List<sObject> recsToUpdate = new List<sObject>();
Integer updateCount = 0;

for (String obj: objects) {
    String myQuery = 'SELECT Id, MigrationID__c FROM ' 
        + obj + ' WHERE MigrationID__c != null';
    List<sObject> queryResults = Database.query(myQuery);
    Integer numRecords = queryResults.size();
    System.debug(obj + ' : ' + numRecords);
    for (sObject oneRec: queryResults) {
    	recsToUpdate.add(oneRec);
//        System.debug(obj + ' Id: ' + oneRec.Id);
    }
}

updateCount = recsToUpdate.size();
System.debug('recsToUpdate.size(): ' + updateCount);

try {
	if (updateCount > 0) {
    	for (sObject rec: recsToUpdate) {
            // System.debug('rec.get(MigrationId) before update: ' + rec.get('MigrationID__c'));
            // System.debug('rec.get(Id): ' + rec.get('Id'));
        	rec.put('MigrationID__c', '');
            // System.debug('rec.get(MigrationId) after update: ' + rec.get('MigrationID__c'));
        }
        update recsToUpdate;
    }else {
        System.debug('No record found. All MigrationID__c fields have been populated.');
    }
}catch (DMLException e) {
    System.debug('Error: ' + e.getMessage());
}
/*******************************************DELETING the FIELD*******************************************************/