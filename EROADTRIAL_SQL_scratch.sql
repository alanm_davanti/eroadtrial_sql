DELETE
FROM EROADTRIAL.dbo.[User]
GO

SELECT Count(*)
FROM EROADTRIAL.dbo.[User]
GO

SELECT *
FROM EROADTRIAL.dbo.UserRole_IDMapping_OldToNew
GO

SELECT EBOX_ID__C, IMEI__C
FROM EROADTRIAL.dbo.Ebox__c
GO

SELECT TOP 1 *
FROM EROADTRIAL.dbo.Machine__c
GO

-- Get a list of tables and views in the current database
SELECT table_catalog [database], table_schema [schema], table_name name, table_type type
FROM INFORMATION_SCHEMA.TABLES
GO

DROP TABLE EROADTRIAL.dbo.Legacyfsb_SCMC__Sales_Order__c
GO