-- -- -- copy all Lookup fields to a new table
-- -- SELECT
-- -- Id,
-- -- NAME,
-- CampaignId, 
-- ContractId, 
-- AccountId, 
-- PartnerAccountId, 
-- Pricebook2Id, 
-- SyncedQuoteId, 
-- Territory2Id, 
-- Partner__c, 
-- Dealer_Organisation__c
-- -- -- Primary_Install_contact__c, 
-- -- -- Preferred_Installer__c
-- -- INTO EROADTRIAL.dbo.NewSB_OpportunityLookups_20190619
-- -- FROM EROADTRIAL.dbo.Opportunity
-- -- GO

-- -- Load data without accounts
-- SELECT count(*)
-- FROM EROADTRIAL.dbo.Opportunity
-- WHERE ACCOUNTID in (SELECT ID FROM EROADTRIAL.dbo.NewSB_AccountLookups_20190604 WHERE New_Id is not null)
-- GO

-- SELECT ID, NAME, ACCOUNTID
-- FROM EROADTRIAL.dbo.Opportunity
-- GO

-- -- select data not loaded
-- SELECT * from EROADTRIAL.dbo.Opportunity
-- WHERE ID not in (select MIGRATIONID__C from EROADTRIAL.dbo.NewSB_Opp_extract)
-- GO

-- -- then remove data in AccountID and Partner__c fields - these will be opportunities without accounts