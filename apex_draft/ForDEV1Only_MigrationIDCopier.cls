public class ForDEV1Only_MigrationIDCopier implements Schedulable {
    public void execute(SchedulableContext ctx) {

        ForDEV1Only_MigrationIDCopyBatch IDcopier = new ForDEV1Only_MigrationIDCopyBatch();
        Id batchId = Database.executeBatch(IDcopier);
        AsyncApexJob job = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID = :batchId ];

    }
}
