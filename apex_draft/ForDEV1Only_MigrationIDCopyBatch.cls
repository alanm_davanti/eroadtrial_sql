public class ForDEV1Only_MigrationIDCopyBatch implements Database.Batchable<sObject>, Database.Stateful {

    String query = 'SELECT Id, AccountID, ContractNumber, Status from Contract ' + 
                               'WHERE CREATEDBYID = \'0056D000000dkZvQAI\' ' +
                                'AND STATUS = \'Draft\' ' + 
        						'ORDER BY ContractNumber';

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Contract> contracts) {
        for (Contract c: contracts) {
            c.Status = 'Activated';
            recordsProcessed = recordsProcessed + 1;
        }
        update contracts;
    }
}
