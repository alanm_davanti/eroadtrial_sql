/*List<AssetAssignedDeviceClass> aDeviceList = new List<AssetAssignedDeviceClass>();
for (integer i=1; i<6; i++) {
    AssetAssignedDeviceClass aDevice1 = new AssetAssignedDeviceClass();
    aDevice1.organisationNumber = 'O-0000055596';
    aDevice1.dispatchDate = System.today();
    //aDevice1.caseID = '00001160'; //Should named as CaseNumber 
    aDevice1.orderNumber = '00000970';
    aDevice1.provisionDate = System.today();
    aDevice1.orderItemNumber = '0000003733';
    aDevice1.itemGroup = 'GROUP A';
    aDevice1.serialNumber = 'Test_Sales_Orch_' + (i);
    aDeviceList.add(aDevice1);
}*/
//Asset Retrieve Device Usage

/* 
List<AssetDeviceUsageClass> aDeviceList = new List<AssetDeviceUsageClass>();

for (Integer i=1; i<6; i++) {
    AssetDeviceUsageClass aDevice = new AssetDeviceUsageClass();
    aDevice.organisationNumber = 'O-0000055596'; //change
    aDevice.productType = 'NZ_PLAN_EH2_Connected_Rental_Heavy'; //change
    aDevice.hardwareRevision = 'Ver2.2';
    aDevice.lifeCycleState = 'INSTALLED';
    aDevice.installDate = System.today();
    aDevice.provisionDate = System.today();
    aDevice.simState = 'Active';
    aDevice.thirdPartyDestinaton = 'some text';
    aDevice.version = 'ver2';
    aDevice.serialNumber = 'Test_Sales_Orch_' + (i); // change
    aDevice.rmaPending = false;
    aDevice.imei = '2';
    aDeviceList.add(aDevice);
    system.debug('aDevice.organisationNumber :: ' + aDevice.organisationNumber);
}

// Assiged Device 
List<CRM_Inbound_Integration__e> ieventList = new List<CRM_Inbound_Integration__e>();
for (AssetDeviceUsageClass ad : aDeviceList) {
    ieventList.add(new CRM_Inbound_Integration__e(
        Message__c = JSON.serialize(ad),
        SObject__c = 'Depot_Asset_Usage',
        Operation__c = ''
    ));
}


List<Database.SaveResult> results = EventBus.publish(ieventList);
// Iterate through each returned result
for (Database.SaveResult sr : results) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted order to platform Event. Contact ID: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Order fields that affected this error: ' + err.getFields());
        }
    }
} */


//MAchine

List<MachineDeviceUsageClass> aDeviceList = new List<MachineDeviceUsageClass>();

for (Integer i=1; i<6; i++) {
    MachineDeviceUsageClass machine = new MachineDeviceUsageClass();
    machine.organisationNumber = 'O-0000055596'; //change
    machine.machineId = 'MachineUniqId_'+(i); //change
    machine.displayName = 'MACHINE_001_' + (i); //change
    machine.registrationPlate = 'ABC_123_' + (i); //change
    machine.assetCode = '108845' + 1;
    machine.servicePlan = 'NZ_PLAN_EH2_Connected_Rental_Heavy'; // change
    machine.machineType = 'TRAILER';
    machine.make = 'Ford';
    machine.model = 'Ford';
    machine.vehicleIdentificationNumber = 'VIN_1234_' + (i);
    machine.yearOfManufacture = '1920';
    machine.serialNumber = 'Test_Sales_Orch_' + (i); //change
    machine.weightType = 'HEAVY';
    machine.fleet = '24';
        
    aDeviceList.add(machine);
    system.debug('machine.organisationNumber :: ' + machine.organisationNumber);
}

// Assiged Device 
List<CRM_Inbound_Integration__e> ieventList = new List<CRM_Inbound_Integration__e>();
for (MachineDeviceUsageClass ad : aDeviceList) {
    ieventList.add(new CRM_Inbound_Integration__e(
        Message__c = JSON.serialize(ad),
        SObject__c = 'Depot_Machine_Usage',
        Operation__c = ''
    ));
}


List<Database.SaveResult> results = EventBus.publish(ieventList);
// Iterate through each returned result
for (Database.SaveResult sr : results) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted order to platform Event. Contact ID: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Order fields that affected this error: ' + err.getFields());
        }
    }
}
