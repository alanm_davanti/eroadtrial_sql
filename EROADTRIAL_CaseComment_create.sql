DROP TABLE IF EXISTS EROADTRIAL.dbo.CaseComment
GO

CREATE TABLE [EROADTRIAL].[dbo].[CaseComment] (
[COMMENTBODY] varchar(max),
[CREATEDBYID] varchar(max),
[CREATEDDATE] varchar(max),
[ID] varchar(20) NOT NULL PRIMARY KEY,
[ISDELETED] varchar(max),
[ISPUBLISHED] varchar(max),
[LASTMODIFIEDBYID] varchar(max),
[LASTMODIFIEDDATE] varchar(max),
[PARENTID] varchar(max),
[SYSTEMMODSTAMP] varchar(max)
)
GO