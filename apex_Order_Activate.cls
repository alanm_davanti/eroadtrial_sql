//Asset Assigned Device
List<AssetAssignedDeviceClass> aDeviceList = new List<AssetAssignedDeviceClass>();
for (integer i=0; i<7; i++) {
    AssetAssignedDeviceClass aDevice1 = new AssetAssignedDeviceClass();
    aDevice1.organisationNumber = 'O-0000021756'; // update this
    aDevice1.dispatchDate = System.today();
    //aDevice1.caseID = '00001160'; //Should named as CaseNumber 
    aDevice1.orderNumber = '00000767'; // update this
    aDevice1.provisionDate = System.today();
    aDevice1.orderItemNumber = '0000002873'; // update this
    aDevice1.itemGroup = 'GROUP A';
    aDevice1.serialNumber = 'Migrated_Q-00059776_' + (i); // update this
    aDeviceList.add(aDevice1);
}
    
// Assiged Device 
List<CRM_Inbound_Integration__e> ieventList = new List<CRM_Inbound_Integration__e>();
for (AssetAssignedDeviceClass aDev : aDeviceList) {
        ieventList.add(new CRM_Inbound_Integration__e(
        Message__c = JSON.serialize(aDev),
        SObject__c = 'Asset_D365',
        Operation__c = ''
    ));
}
List<Database.SaveResult> results = EventBus.publish(ieventList);


// Iterate through each returned result
for (Database.SaveResult sr : results) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted asset to platform Event. PlatformId ID: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Asset fields that affected this error: ' + err.getFields());
        }
    }
}
