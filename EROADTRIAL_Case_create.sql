DROP TABLE IF EXISTS EROADTRIAL.dbo.[Case]
GO

CREATE TABLE [EROADTRIAL].[dbo].[Case] (
[ACCESSORIES__C] varchar(max),
[ACCESSORIES_ONLY__C] varchar(max),
[ACCESSORIES_REQUIRED__C] varchar(max),
[ACCOUNTID] varchar(max),
[ACCOUNTS_QUERY__C] varchar(max),
[ACCOUNTS_TYPE__C] varchar(max),


[ANZ_CS_RESOLUTION_ON_HOLD_REASON__C] varchar(max),
[ANZ_GS_RESOLUTION_ON_HOLD_REASON__C] varchar(max),


[ARE_YOU_AFFECTED_BY_THE_ERROR__C] varchar(max),
[ASSETID] varchar(max),
[AT_RISK_CUSTOMER__C] varchar(max),

[BUSINESSHOURSID] varchar(max),
[CASE_AGE_IN_BUSINESS_HOURS__C] varchar(max),
[CASE_ASSIGNED_TIME__C] varchar(max),


[CASE_TIME_IN_QUEUE__C] varchar(max),



[CASENUMBER] varchar(max),


[CHECKED_OFF_ROAD_REFUNDS_AND_CURRENT_RUC__C] varchar(max),
[CHECKED_PLANS__C] varchar(max),
[CLOSED_BY__C] varchar(max),
[CLOSEDDATE] varchar(max),
[COMMENTS] varchar(max),
[COMPLAINT__C] varchar(max),
[COMPLAINT_CATEGORY__C] varchar(max),
[COMPLAINT_DETAILS__C] varchar(max),
[COMPLAINT_REASON__C] varchar(max),
[CONCLUSION__C] varchar(max),
[CONTACTEMAIL] varchar(max),
[CONTACTFAX] varchar(max),
[CONTACTID] varchar(max),
[CONTACTMOBILE] varchar(max),
[CONTACTPHONE] varchar(max),


[CREATEDBYID] varchar(max),
[CREATEDDATE] varchar(max),


[CURRENCYISOCODE] varchar(max),
[CUSTOMER_SUCCESS_QUERY__C] varchar(max),
[CUSTOMER_SUCCESS_TYPE__C] varchar(max),

[DATE_RETURNED__C] varchar(max),
[DATE_SHIPPED__C] varchar(max),
[DATE_TIME_ISSUE_OCCURRED__C] varchar(max),
[DATE_TIME_LAST_CLOSED__C] varchar(max),
[DATE_TIME_RE_OPENED__C] varchar(max),
[DAYOFTHEWEEK4LASTMODIFIEDDT__C] varchar(max),
[DAYS_TO_RETURN_UNIT__C] varchar(max),
[DD_FORMS__C] varchar(max),



[DEPOT_FAULT_REPORT_ID__C] varchar(max),
[DEPOT_LOCAL_REPORT_ID__C] varchar(max),



[DEPOT_STATUS__C] varchar(max),
[DESCRIPTION] varchar(max),
[DEVICE_SERIAL_NUMBER__C] varchar(max),

[DISCUSSED_RENEWAL__C] varchar(max),





[ELAPSED_DAYS__C] varchar(max),



[EXPLORED_GROWTH_POTENTIAL__C] varchar(max),
[EXTERNAL_LEAD_ACID_BATTERY__C] varchar(max),
[FAILED_ON_INSTALL__C] varchar(max),
[FAULT_DESCRIPTION_BY_SERVICE_CENTRE__C] varchar(max),
[FAULT_FOUND_IN_FACTORY__C] varchar(max),
[FAULTS_FOUND_LEVEL_1__C] varchar(max),
[FAULTS_FOUND_LEVEL_2__C] varchar(max),
[FAULTS_FOUND_LEVEL_3__C] varchar(max),




[FIRST_TIME_SOLVED__C] varchar(max),


[FOLLOW_UP_DATE__C] varchar(max),

[FUTURE_POTENTIAL_STATUS__C] varchar(max),






[GOODS_IN_QUARANTINE__C] varchar(max),





[I_NEED_ASSISTANCE_WITH__C] varchar(max),
[ICCID__C] varchar(max),
[ID] varchar(20) NOT NULL PRIMARY KEY,








[INITIATOR__C] varchar(max),
[INSTALLER__C] varchar(max),

[INSTALLER_VISIT_REQUESTED__C] varchar(max),

[INTERNAL_LITHIUM_ION_BATTERY_KIT__C] varchar(max),
[ISCLOSED] varchar(max),
[ISCLOSEDONCREATE] varchar(max),
[ISDELETED] varchar(max),
[ISESCALATED] varchar(max),



[JIRA_ASSIGNEE__C] varchar(max),
[JIRA_DESCRIPTION__C] varchar(max),
[JIRA_FIXVERSION__C] varchar(max),
[JIRA_LABELS__C] varchar(max),
[JIRA_PBREVIEW_DUE_DATE__C] varchar(max),
[JIRA_RESOLUTION__C] varchar(max),
[JIRA_SUMMARY__C] varchar(max),
[JIRA_TICKET_NUMBER__C] varchar(max),

[LAST_STATUS_CHANGE__C] varchar(max),


[LASTMODIFIEDBYID] varchar(max),
[LASTMODIFIEDDATE] varchar(max),
[LASTREFERENCEDDATE] varchar(max),
[LASTVIEWEDDATE] varchar(max),












[NEW_CONCLUSION__C] varchar(max),
















[ON_HOLD_REASON__C] varchar(max),
[OPS_NOTES__C] varchar(max),

[ORIGIN] varchar(max),








[OWNERID] varchar(max),
[PARENTID] varchar(max),


[PIGGY_BACK__C] varchar(max),



[PRIORITY] varchar(max),
[PRODUCT__C] varchar(max),
[PRODUCT_PRICE__C] varchar(max),


[PROVIDED_OVERVIEW_OF_NEW_H_W_FEATURES__C] varchar(max),
[PROVIDED_OVERVIEW_OF_NEW_PLAN_FEATURES__C] varchar(max),
[QUERY_FAULT_TYPE__C] varchar(max),



[REASON] varchar(max),
[REASON__C] varchar(max),

[RECORDTYPEID] varchar(max),

[RELATED_QUOTE_NUMBER__C] varchar(max),
[RELATED_SALES_ORDER__C] varchar(max),


[RENEWAL_STATUS__C] varchar(max),
[RENEWALS_RESOLUTION_ON_HOLD_REASON__C] varchar(max),
[REPORTED_FAULT_REASON_S__C] varchar(max),

[REPORTED_FAULT_SYMPTOMS__C] varchar(max),


[REWORK_DESCRIPTION__C] varchar(max),
[RMA__C] varchar(max),
[RMA_1ST_TEST_RESULT__C] varchar(max),
[RMA_DETAILS__C] varchar(max),
[RMA_INSTALLER__C] varchar(max),
[RMA_TYPE__C] varchar(max),





[SERVICE_DELIVERY_QUERY__C] varchar(max),










[SIM_CARD_REPLACEMENT__C] varchar(max),
[SITE_CONTACT_NAME__C] varchar(max),
[SITE_CONTACT_PHONE__C] varchar(max),

[SOURCEID] varchar(max),


[STATUS] varchar(max),
[SUBJECT] varchar(max),
[SUPPLIEDCOMPANY] varchar(max),
[SUPPLIEDEMAIL] varchar(max),
[SUPPLIEDNAME] varchar(max),
[SUPPLIEDPHONE] varchar(max),
[SVC_DEL_TYPE__C] varchar(max),

[SYSTEMMODSTAMP] varchar(max),
[T2_JIRA_TICKET_NUMBER__C] varchar(max),
[T3_ISSUE_TYPE__C] varchar(max),
[T3_PRODUCT__C] varchar(max),

[TAMPER_DETECTED__C] varchar(max),





[TIER_1_COMPONENT__C] varchar(max),
[TIER_1_CONCLUSION__C] varchar(max),
[TIER_1_ISSUE_TYPE__C] varchar(max),
[TIER_1_PRODUCT__C] varchar(max),


[TIER_2_COMPONENT__C] varchar(max),
[TIER_2_ISSUE_TYPE__C] varchar(max),
[TIER_2_NOTES__C] varchar(max),
[TIER_2_OWNER__C] varchar(max),
[TIER_2_PRODUCT__C] varchar(max),
[TIER_2_RESOLUTION__C] varchar(max),

[TIER_2_STATUS__C] varchar(max),

[TIER_3_COMPONENT__C] varchar(max),
[TIER_3_OWNER__C] varchar(max),
[TIER_3_RESOLUTION__C] varchar(max),
[TIER_3_STATUS__C] varchar(max),

[TIME_WITH_CUSTOMER__C] varchar(max),
[TIME_WITH_SUPPORT__C] varchar(max),

[TRAINING__C] varchar(max),
[TRAINING_COMPLETED__C] varchar(max),
[TRAINING_DATE_SCHEDULED__C] varchar(max),


[TYPE] varchar(max),

[UNIT_IN_WARRANTY__C] varchar(max),
[UNIT_NOT_REPORTING__C] varchar(max),
[UNIT_RETIRED_ON_DEPOT__C] varchar(max),

[USER_LIST__C] varchar(max),
[VALIDATED_SALESFORCE_CONTACT_INFO__C] varchar(max),
[VALIDATED_SALESFORCE_INDUSTRY_INFO__C] varchar(max),
[VEHICLE_LIST__C] varchar(max),






[X2ND_LEVEL_NOTES__C] varchar(max)



)
GO