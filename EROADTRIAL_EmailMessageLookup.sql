-- REPLYTOEMAILMESSAGEID field cannot be updated if the Status is not 'Draft'
-- https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_emailmessage.htm
SELECT 
ID as MIGRATIONID__C,
REPLYTOEMAILMESSAGEID
FROM EROADTRIAL.dbo.EmailMessage
WHERE REPLYTOEMAILMESSAGEID != ''
GO