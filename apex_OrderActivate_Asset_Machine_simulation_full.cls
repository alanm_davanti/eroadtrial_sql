
//SOQL Query to guide execution:
//select id,product_family__c, product2.productcode, product2.external_depot_code__c, orderitemNumber, Order.Account.Organisation_Number__c, order.ordernumber,  SBQQ__Subscription__c, SBQQ__Asset__c,  SBQQ__Status__c, Order.Status, Quantity, (SELECT ID, Service_Contract__c, Service_Contract__r.ContractNumber, D365_Asset_Number__c, SerialNumber FROM SBQQ__Assets__r) from Orderitem where orderId = '8010l000000GJAlAAO'

/***********************Assigned Device INT011 - START*****************************************/
//Asset Assigned Device
List<AssetAssignedDeviceClass> aDeviceList = new List<AssetAssignedDeviceClass>();
for (integer i=1; i<6; i++) {
    AssetAssignedDeviceClass aDevice1 = new AssetAssignedDeviceClass();
    aDevice1.organisationNumber = 'O-0000055625'; // update this
    aDevice1.dispatchDate = System.today();
    //aDevice1.caseID = '00001160'; //Should named as CaseNumber 
    aDevice1.orderNumber = '00001019'; // update this with the orderNumber
    aDevice1.provisionDate = System.today();
    aDevice1.orderItemNumber = '0000003963'; // update this, OrderItemNumber of Hardware
    aDevice1.itemGroup = 'GROUP A';
    aDevice1.serialNumber = 'Q-01030_2_' + (i); // update this to unique value
    aDeviceList.add(aDevice1);
}
    
// Assiged Device 
List<CRM_Inbound_Integration__e> ieventList = new List<CRM_Inbound_Integration__e>();
for (AssetAssignedDeviceClass aDev : aDeviceList) {
        ieventList.add(new CRM_Inbound_Integration__e(
        Message__c = JSON.serialize(aDev),
        SObject__c = 'Asset_D365',
        Operation__c = ''
    ));
}
List<Database.SaveResult> results = EventBus.publish(ieventList);


// Iterate through each returned result
for (Database.SaveResult sr : results) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted asset to platform Event. PlatformId ID: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Asset fields that affected this error: ' + err.getFields());
        }
    }
}

/******************Alternative to INT011 create arbitrary number of assets - START ***************************/
List<Orderitem> lstOrderitem = [select id,product_family__c, product2.productcode, product2.external_depot_code__c,
                            orderitemNumber, Order.Account.Organisation_Number__c, order.ordernumber,  SBQQ__Subscription__c, SBQQ__Asset__c,  
                            SBQQ__Status__c, Order.Status, Quantity 
                            from Orderitem 
                            where orderId = '8010l000000PHlZAAW' AND Product_Family_Text__c = 'Hardware']; // change OrderId

List<String> serialNumbers = new List<String>();
List<AssetAssignedDeviceClass> lstAssetAssignedDevice = new List<AssetAssignedDeviceClass>();
            
if(lstOrderitem.size() > 0){
    for(integer index = 1; index < 101; index++){ // set arbitrary number of assets here
        OrderItem orderProduct = lstOrderitem[0];                    
        AssetAssignedDeviceClass aDevice1 = new AssetAssignedDeviceClass();
        aDevice1.organisationNumber = orderProduct.Order.Account.Organisation_Number__c; // automatically queried
        aDevice1.dispatchDate = System.today();
        aDevice1.orderNumber = orderProduct.order.ordernumber; // automatically queried
        aDevice1.provisionDate = System.today();
        aDevice1.orderItemNumber = orderProduct.orderitemNumber; // automatically queried
        aDevice1.itemGroup = 'GROUP A';
        aDevice1.serialNumber = orderProduct.order.ordernumber + '_' + orderProduct.orderitemNumber + '_' + index; // serial number should be unique
        
        system.debug('*****serialNumber***** => ' + aDevice1.serialNumber);
        serialNumbers.add(aDevice1.serialNumber);
        lstAssetAssignedDevice.add(aDevice1);
    }
    // publish to platform event
    // Assiged Device 
    List<CRM_Inbound_Integration__e> ieventList = new List<CRM_Inbound_Integration__e>();
    for (AssetAssignedDeviceClass aDev : lstAssetAssignedDevice) {
            ieventList.add(new CRM_Inbound_Integration__e(
            Message__c = JSON.serialize(aDev),
            SObject__c = 'Asset_D365',
            Operation__c = ''
        ));
    }
    List<Database.SaveResult> results = EventBus.publish(ieventList);
    // Iterate through each returned result
    for (Database.SaveResult sr : results) {
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully inserted asset to platform Event. PlatformId ID: ' + sr.getId());
        }
        else {
            // Operation failed, so get all errors                
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Asset fields that affected this error: ' + err.getFields());
            }
        }
    }
   
} else {
System.debug('No Order identified for this simulation.');
}
/******************Alternative to INT011 create arbitrary number of assets  - END ***************************/

/***********************Assigned Device INT011 - END *****************************************/


/***********************Retrieve Device Usage (Asset) INT029 - START *****************************************/
//Asset Retrieve Device Usage

List<AssetDeviceUsageClass> aDeviceList = new List<AssetDeviceUsageClass>();

for (Integer i=1; i<6; i++) {
    AssetDeviceUsageClass aDevice = new AssetDeviceUsageClass();
    aDevice.organisationNumber = 'O-0000055625'; // change
    aDevice.productType = 'NZ_PLAN_ConnectedTrack'; //change product2.external_depot_code__c
    aDevice.hardwareRevision = 'Ver2.2';
    aDevice.lifeCycleState = 'Installed';  //keep as Installed
    aDevice.installDate = System.today();
    aDevice.provisionDate = System.today();
    aDevice.simState = 'Active';
    aDevice.thirdPartyDestinaton = 'some text';
    aDevice.version = 'ver2';
    aDevice.serialNumber = 'Q-01030_2_' + (i); //change, same as serial number of asset generated above
    aDevice.rmaPending = false;
    aDevice.imei = '2';
    aDeviceList.add(aDevice);
    system.debug('aDevice.organisationNumber :: ' + aDevice.organisationNumber);
}

// Assiged Device 
List<CRM_Inbound_Integration__e> ieventList = new List<CRM_Inbound_Integration__e>();
for (AssetDeviceUsageClass ad : aDeviceList) {
    ieventList.add(new CRM_Inbound_Integration__e(
        Message__c = JSON.serialize(ad),
        SObject__c = 'Depot_Asset_Usage',
        Operation__c = ''
    ));
}


List<Database.SaveResult> results = EventBus.publish(ieventList);
// Iterate through each returned result
for (Database.SaveResult sr : results) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted order to platform Event. Contact ID: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Order fields that affected this error: ' + err.getFields());
        }
    }
} 
/***********************Retrieve Device Usage (Asset) INT029 - END *****************************************/


/***********************Retrieve Device Usage (Machine) INT029 - START *****************************************/
//MAchine

List<MachineDeviceUsageClass> aDeviceList = new List<MachineDeviceUsageClass>();

for (Integer i=1; i<6; i++) {
    MachineDeviceUsageClass machine = new MachineDeviceUsageClass();
    machine.organisationNumber = 'O-0000055625'; //change
    machine.machineId = 'SalesOrch3TestId_'+(i); //change - this is a unique ID
    machine.displayName = 'SOT3_123_' + (i); //change
    machine.registrationPlate = 'SOT3_123_' + (i); //change - Upsert is concatenation of Org Number and rego
    machine.assetCode = '108845' + 1;
    machine.servicePlan = 'NZ_PLAN_ConnectedTrack'; // change to product2.external_depot_code__c
    machine.machineType = 'Tubo';
    machine.make = 'Ford';
    machine.model = 'Ford';
    machine.vehicleIdentificationNumber = 'VIN_1234_' + (i);
    machine.yearOfManufacture = '1920';
    machine.serialNumber = 'Q-01030_2_' + (i); //change
    machine.weightType = 'HEAVY';
    machine.fleet = '24';
        
    aDeviceList.add(machine);
    system.debug('machine.organisationNumber :: ' + machine.organisationNumber);
}

// Assiged Device 
List<CRM_Inbound_Integration__e> ieventList = new List<CRM_Inbound_Integration__e>();
for (MachineDeviceUsageClass ad : aDeviceList) {
    ieventList.add(new CRM_Inbound_Integration__e(
        Message__c = JSON.serialize(ad),
        SObject__c = 'Depot_Machine_Usage',
        Operation__c = ''
    ));
}


List<Database.SaveResult> results = EventBus.publish(ieventList);
// Iterate through each returned result
for (Database.SaveResult sr : results) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted order to platform Event. Contact ID: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Order fields that affected this error: ' + err.getFields());
        }
    }
} 

/***********************Retrieve Device Usage (Machine) INT029 - END *****************************************/