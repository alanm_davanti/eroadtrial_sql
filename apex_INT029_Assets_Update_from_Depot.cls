//Asset Retrieve Device Usage

List<AssetDeviceUsageClass> aDeviceList = new List<AssetDeviceUsageClass>();

for (Integer i=1; i<2; i++) {
    AssetDeviceUsageClass aDevice = new AssetDeviceUsageClass();
    aDevice.organisationNumber = 'O-0000055566';
    aDevice.productType = 'GL_AO_FT_ProofofService_Usage';
    aDevice.hardwareRevision = 'Ver2.2';
    aDevice.lifeCycleState = 'Active';
    aDevice.installDate = System.today();
    aDevice.provisionDate = System.today();
    aDevice.simState = 'Active';
    aDevice.thirdPartyDestinaton = 'some text';
    aDevice.version = 'ver2';
    aDevice.serialNumber = 'Usage_Ser_1';
    aDevice.rmaPending = false;
    aDevice.imei = '2';
    aDeviceList.add(aDevice);
    system.debug('aDevice.organisationNumber :: ' + aDevice.organisationNumber);
}

// Assiged Device 
List<CRM_Inbound_Integration__e> ieventList = new List<CRM_Inbound_Integration__e>();
for (AssetDeviceUsageClass ad : aDeviceList) {
    ieventList.add(new CRM_Inbound_Integration__e(
        Message__c = JSON.serialize(ad),
        SObject__c = 'Depot_Asset_Usage',
        Operation__c = ''
    ));
}


List<Database.SaveResult> results = EventBus.publish(ieventList);
// Iterate through each returned result
for (Database.SaveResult sr : results) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted order to platform Event. Contact ID: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Order fields that affected this error: ' + err.getFields());
        }
    }
}