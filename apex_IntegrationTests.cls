
//DEV1:
CaseEvent_SyncCase.CaseRecordWrapper caseEventWrap = new CaseEvent_SyncCase.CaseRecordWrapper();
caseEventWrap.isNew = false;
caseEventWrap.organisationNumber = 'O-0000055623';
Set<Id> caseIds = new Set<Id>();
caseIds.add('5000l000005pstVAAQ');
caseEventWrap.caseRecord = CaseService.getCasesByCaseIdSet(caseIds)[0];
caseEventWrap.eventType = 'UpdateContract';
List<CaseEvent_SyncCase.CaseRecordWrapper> caseEventWrapList = new List<CaseEvent_SyncCase.CaseRecordWrapper>();
caseEventWrapList.add(caseEventWrap);
IntegrationService.getInstance().synchronizeCase(caseEventWrapList);


//TEST1:
CaseEvent_SyncCase.CaseRecordWrapper caseEventWrap = new CaseEvent_SyncCase.CaseRecordWrapper();
caseEventWrap.isNew = false;
caseEventWrap.organisationNumber = 'O-0000065112';
Set<Id> caseIds = new Set<Id>();
caseIds.add('5000l000005puexAAA');
caseEventWrap.caseRecord = CaseService.getCasesByCaseIdSet(caseIds)[0];
caseEventWrap.eventType = 'UpdateContract';
List<CaseEvent_SyncCase.CaseRecordWrapper> caseEventWrapList = new List<CaseEvent_SyncCase.CaseRecordWrapper>();
caseEventWrapList.add(caseEventWrap);
IntegrationService.getInstance().synchronizeCase(caseEventWrapList);

private Map<String, List<Product2>> productCodetoProductIdMap = new Map<String, List<Product2>>();
Set<String> productCodeSet = new Set<String>();
productCodeSet.add('NZ_PLAN_EH2_Connected');
productCodeSet.add('NZ_PLAN_EH2_ConnectedPlus');
List<Product2> queryResult = ProductService.getProductsForAssignedDeviceByProductCode(productCodeSet);
System.debug(queryResult.size());

        if (productCodeSet != null && productCodeSet.size() > 0) {
            //for (Product2 prod2 : [Select Id, ProductCode From Product2 Where ProductCode IN :productCodeSet]) {
            for (String extDepotCode: productCodeSet) {
                productCodetoProductIdMap.put(extDepotCode, new List<Product2>());
                                
                /*for (Product2 prod2 : queryResult) {
                //this.productCodetoProductIdMap.put(prod2.ProductCode, prod2.Id);
                
                //2-Nov: Using Depot Productcodeinstead
                if (prod2.External_Depot_Code__c == extDepotCode) {
                	productCodetoProductIdMap.put(extDepotCode, productCodetoProductIdMap.get(extDepotCode).add(prod2));    
                }*/
                
            }
                    
            System.debug(productCodetoProductIdMap);
            //System.debug(productCodetoProductIdMap.keyset().get(0));
            //System.debug(productCodetoProductIdMap.keyset().get(1));
            //productCodetoProductIdMap.put(productCodetoProductIdMap.keyset()[0], productCodetoProductIdMap.get(productCodetoProductIdMap.keyset()[0]).add(queryResult[0]));
            
            for (Product2 prod2 : queryResult) {
                //this.productCodetoProductIdMap.put(prod2.ProductCode, prod2.Id);
                
                //2-Nov: Using Depot Productcodeinstead
             
                	productCodetoProductIdMap.get(prod2.External_Depot_Code__c).add(prod2);    
                               
            }
        }

/*for (Product2 prod2 : queryResult) {
    System.debug('prod2::' + prod2);
    productCodetoProductIdMap.put(prod2.External_Depot_Code__c, prod2.Id);
}*/
System.debug(productCodetoProductIdMap);
for (String prodCode: productCodetoProductIdMap.keySet()) {
    System.debug(productCodeToProductIdMap.get(prodCode).size());
}