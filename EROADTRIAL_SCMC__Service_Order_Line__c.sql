
SELECT

-- ISNULL(CREATEDBYID,'') AS CREATEDBYID, -- uncomment if Users have been loaded
ISNULL(ServiceContractLine.CREATEDDATE,'') AS CREATEDDATE,
ISNULL(ServiceContractLine.CURRENCYISOCODE,'') AS CURRENCYISOCODE,
ISNULL(ServiceContractLine.ID,'') AS MIGRATIONID__C,
ISNULL(ServiceContractLine.ISDELETED,'') AS ISDELETED,

-- ISNULL(LASTMODIFIEDBYID,'') AS LASTMODIFIEDBYID, -- uncomment if Users have been loaded
ISNULL(ServiceContractLine.LASTMODIFIEDDATE,'') AS LASTMODIFIEDDATE,
ISNULL(ServiceContractLine.NAME,'') AS NAME,
ISNULL(ServiceContractLine.SCMC__QUANTITY_DELIVERED__C,'') AS SBQQ__QUANTITY__C,
ISNULL(ServiceContractLine.SCMC__QUANTITY_DELIVERED__C,'') AS APPROVED_QUANTITY__C,

-- 'TRUE' AS PRINT__C, -- PRINT__C is a formula field
-- ISNULL(ServiceContractLine.CREATEDBYID, '') AS OWNERID, -- uncomment if users have been loaded
'FALSE' AS PILOT__C,

-- ISNULL(ServiceContract.SCMC__CUSTOMER_ACCOUNT_SOLD_TO__C,'') AS ACCOUNTID, -- uncomment if all Accounts have been loaded
'0019000000w7BjvAAE' AS SBQQ__ACCOUNT__C, -- for upload test only, change to appropriate Organisation ID

'' as SBQQ__ADDITIONALDISCOUNTAMOUNT__C,
ISNULL('Monthly','') as SBQQ__BILLINGFREQUENCY__C,
ISNULL('Advance','') as SBQQ__BILLINGTYPE__C,
ISNULL('FALSE','') as SBQQ__BUNDLE__C, -- to be updated as per product bundling configuration
ISNULL('FALSE','') as SBQQ__BUNDLED__C, -- to be updated as per product bundling configuration
ISNULL('','') as SBQQ__BUNDLEDQUANTITY__C, -- to be updated as per product bundling configuration
ISNULL('Recurring','') as SBQQ__CHARGETYPE__C, -- to be updated. Picklist values are Recurring, One-Time and Usage
'' as SBQQ__COMPONENTDISCOUNTEDBYPACKAGE__C,
'' as SBQQ__COMPONENTSUBSCRIPTIONSCOPE__C,
ISNULL(ServiceContractLine.SCMC__SERVICE_ORDER__C,'') as SBQQ__CONTRACT__C,
ISNULL(ServiceContractLine.SCMC__RC_AFTER_DISCOUNT__C,'') as SBQQ__CUSTOMERPRICE__C,
'' as SBQQ__DIMENSION__C,
'' as SBQQ__DIMENSIONTYPE__C,
'' as SBQQ__DISCOUNT__C,
'' as SBQQ__DISCOUNTSCHEDULE__C,
'' as SBQQ__DISCOUNTSCHEDULETYPE__C,
'' as SBQQ__DISTRIBUTORDISCOUNT__C,
ISNULL('','') as SBQQ__DYNAMICOPTIONID__C, -- to be updated with Product Feature ID and optional SKU if applicable
ISNULL(ServiceContractLine.SCMC__RC_AFTER_DISCOUNT__C,'') as SBQQ__LISTPRICE__C,
'' as SBQQ__MARKUPAMOUNT__C,
'' as SBQQ__MARKUPRATE__C,
ISNULL(ServiceContractLine.SCMC__RC_AFTER_DISCOUNT__C,'') as SBQQ__NETPRICE__C,
ISNULL('','') as SBQQ__NUMBER__C, -- to be updated as per product bundling configuration
'' as SBQQ__OPTIONDISCOUNT__C,
'' as SBQQ__OPTIONDISCOUNTAMOUNT__C,
ISNULL('','') as SBQQ__OPTIONLEVEL__C, -- to be updated as per product bundling configuration
ISNULL('','') as SBQQ__OPTIONTYPE__C, -- to be updated as per product bundling configuration
ISNULL(ServiceContractLine.SCMC__SALES_ORDER_LINE_ITEM__C,'') as SBQQ__ORDERPRODUCT__C,
'List' as SBQQ__PRICINGMETHOD__C,

-- Note: replace below with updated mapping from old product to new product
CASE 
    WHEN ServiceContractLine.SCMC__ITEM_NUMBER__C = 'a4G90000000GoOlEAK' THEN '01t0l000002wx6FAAQ'
    WHEN ServiceContractLine.SCMC__ITEM_NUMBER__C = 'a4G90000000GoOuEAK' THEN '01t0l00000332hsAAA'
    WHEN ServiceContractLine.SCMC__ITEM_NUMBER__C = 'a4G90000000kI8KEAU' THEN '01t0l000002wx5bAAA'
    WHEN ServiceContractLine.SCMC__ITEM_NUMBER__C = 'a4G90000000kJk5EAE' THEN '01t0l000002wx6pAAA'
    ELSE ''
END AS SBQQ__PRODUCT__C,

ISNULL('','') as SBQQ__PRODUCTOPTION__C, -- to be updated as per product bundling configuration
ISNULL(ServiceContractLine.SCMC__TOTAL_SERVICE_TERMS__C,'') as SBQQ__PRORATEMULTIPLIER__C,

'' as SBQQ__QUOTELINE__C,
ISNULL('','') as SBQQ__REGULARPRICE__C,
ISNULL('','') as SBQQ__RENEWALPRICE__C,
ISNULL(ServiceContractLine.SCMC__QUANTITY_DELIVERED__C,'') as SBQQ__RENEWALQUANTITY__C,
'' as SBQQ__RENEWALUPLIFTRATE__C,
'' as SBQQ__RENEWEDDATE__C,
ISNULL('','') as SBQQ__REQUIREDBYID__C, -- to be updated as per product bundling configuration
ISNULL('','') as SBQQ__REQUIREDBYPRODUCT__C, -- to be updated as per product bundling configuration
'' as SBQQ__REVISEDSUBSCRIPTION__C,
'' as SBQQ__ROOTID__C, -- to be updated as per product bundling configuration
'' as SBQQ__SEGMENTENDDATE__C,
'' as SBQQ__SEGMENTINDEX__C,
'' as SBQQ__SEGMENTKEY__C,
'' as SBQQ__SEGMENTLABEL__C,
'' as SBQQ__SEGMENTQUANTITY__C,
'' as SBQQ__SEGMENTSTARTDATE__C,
'' as SBQQ__SEGMENTUPLIFT__C,
'' as SBQQ__SEGMENTUPLIFTAMOUNT__C,
ISNULL('','') as SBQQ__SPECIALPRICE__C,
ISNULL(ServiceContractLine.SCMC__SERVICE_CONTRACT_LINE_END_DATE__C,'') as SBQQ__SUBSCRIPTIONENDDATE__C,
ISNULL(ServiceContractLine.SCMC__SERVICE_CONTRACT_LINE_START_DATE__C,'') as SBQQ__SUBSCRIPTIONSTARTDATE__C,
ISNULL('','') as SBQQ__TERMDISCOUNTSCHEDULE__C,
ISNULL('','') as SBQQ__TERMINATEDDATE__C,
ISNULL('','') as SBQQ__UNITCOST__C,
'Renewable' AS SBQQ__PRODUCTSUBSCRIPTIONTYPE__C, -- to be updated if Contract is Renewable or Evergreen
'Renewable' AS SBQQ__SUBSCRIPTIONTYPE__C -- to be updated if Contract is Renewable or Evergreen

FROM EROADTRIAL.dbo.SCMC__Service_Order_Line__c AS ServiceContractLine
INNER JOIN EROADTRIAL.dbo.SCMC__Service_Order__c AS ServiceContract
ON ServiceContractLine.SCMC__SERVICE_ORDER__C = ServiceContract.ID
WHERE ServiceContract.SCMC__SALES_ORDER__C in ('a520o000000lI1PAAU', 
'a520o000000lI1AAAU', 
'a520o000000lI10AAE', 
'a520o000000lI0qAAE', 
'a520o000000lI0lAAE', 
'a520o000000lI0bAAE', 
'a520o000000lI0WAAU', 
'a520o000000lI0RAAU', 
'a520o000000lI0MAAU', 
'a520o000000lI0HAAU')
GO